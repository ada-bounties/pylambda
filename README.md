# PyLambda

These lambda modules are used within AWS Lambda. Most are designed to function like "cron" jobs asynchronously from the ADA Bounties servers.

## Functions and layouts

<b>SSL Check Expiry</b>
<ul>
<li><i class="fas fa-caret-right"></i><li>Checks the expiration of the Elasticsearch server certificate and sends an email with the number of valid days left.</li>
</ul>


